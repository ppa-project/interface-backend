# Copyright 2021 TNO
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

basePath: /
definitions:
  coordinator.LogEntry:
    properties:
      contents:
        description: |-
          Contents is always one of the LogEntry___ types defined in this package
          and always corresponds to the value of Kind
        type: object
      kind:
        example: 1
        type: integer
      timestamp:
        example: "2020-11-30T10:36:28Z"
        type: string
    type: object
  coordinator.LogEntryPresenceGUI:
    properties:
      cert:
        example: '-----BEGIN CERTIFICATE----- ... -----END CERTIFICATE-----'
        type: string
      ip:
        example: 2.106.3.94
        type: string
      organization:
        example: ACME
        type: string
    type: object
  coordinator.LogEntryPresenceMPC:
    properties:
      attributes:
        items:
          $ref: '#/definitions/types.DatabaseAttribute'
        type: array
      cert:
        example: '-----BEGIN CERTIFICATE----- ... -----END CERTIFICATE-----'
        type: string
      dbHash:
        example: 861f4ec2bd03b200a0ed11e7156e1206ab47ee7da6fe964303f10c7764ad8d14
        type: string
      ip:
        example: 2.106.3.94
        type: string
      organization:
        example: ACME
        type: string
      paillierkey:
        example: '{"N":8972389472389478 ....}'
        type: string
      port:
        example: "443"
        type: string
    type: object
  coordinator.LogEntryQueryApproved:
    properties:
      metadata:
        $ref: '#/definitions/coordinator.QueryMetadata'
      query:
        $ref: '#/definitions/types.TaggedQuery'
      sender:
        example: 0xcbc0deda74565cce14ddd6d581d84970ef66e34d
        type: string
    type: object
  coordinator.LogEntryQueryComplete:
    properties:
      queryID:
        example: be05377deca950d2338d501ec948f00361d203b3fb55815303d7e5d8a0690163
        type: string
      resultHash:
        example: 8aa253608c981d8d9b3130e7f4d8a60c9b491d2d93a454475a505b91bb6f9b29
        type: string
      sender:
        example: 0xcbc0deda74565cce14ddd6d581d84970ef66e34d
        type: string
      successful:
        example: true
        type: boolean
    type: object
  coordinator.LogEntryQueryManualReview:
    properties:
      queryID:
        example: be05377deca950d2338d501ec948f00361d203b3fb55815303d7e5d8a0690163
        type: string
      reviewer:
        example: PartyC
        type: string
      sender:
        example: 0xcbc0deda74565cce14ddd6d581d84970ef66e34d
        type: string
    type: object
  coordinator.LogEntryQueryRejected:
    properties:
      query:
        $ref: '#/definitions/types.Query'
      reason:
        example: Query does not comply to the businessrules
        type: string
      sender:
        example: 0xcbc0deda74565cce14ddd6d581d84970ef66e34d
        type: string
    type: object
  coordinator.LogEntryQueryUnaffordable:
    properties:
      budgetRemaining:
        example: 1000
        type: integer
      cost:
        example: 30
        type: integer
      query:
        $ref: '#/definitions/types.Query'
      sender:
        example: 0xcbc0deda74565cce14ddd6d581d84970ef66e34d
        type: string
    type: object
  coordinator.NewQueryMetadata:
    properties:
      destination_nodes:
        items:
          type: string
        type: array
      groupkey:
        type: string
    type: object
  coordinator.PercentageConstraint:
    properties:
      attribute:
        example: attribute1
        type: string
      maxPercentage:
        example: 85
        type: integer
      minPercentage:
        example: 2
        type: integer
      owner:
        example: ACME
        type: string
    type: object
  coordinator.QueryMetadata:
    properties:
      destinationNodes:
        items:
          type: string
        type: array
      groupkey:
        type: string
      maxResultPercentage:
        example: 75
        type: integer
      minResultPercentage:
        example: 1
        type: integer
      minResultSize:
        example: 45
        type: integer
      percentageConstraints:
        items:
          $ref: '#/definitions/coordinator.PercentageConstraint'
        type: array
      reviewer:
        type: string
      stdevConstraints:
        items:
          $ref: '#/definitions/coordinator.StdevConstraint'
        type: array
    type: object
  coordinator.StdevConstraint:
    properties:
      attribute:
        example: attribute1
        type: string
      minStdev:
        example: 0.1
        type: number
      owner:
        example: ACME
        type: string
    type: object
  messages.JSONApprovalRequestBasicStats:
    properties:
      aggregates:
        items:
          type: number
        type: array
      count:
        type: integer
      query:
        $ref: '#/definitions/types.TaggedQuery'
    type: object
  messages.JSONApprovalRequestSummary:
    properties:
      completed_timestamp:
        type: string
      creation_timestamp:
        type: string
      queryID:
        type: string
      status:
        type: string
      summary:
        type: string
    type: object
  messages.JSONApprovalRequests:
    properties:
      approvalRequests:
        items:
          $ref: '#/definitions/messages.JSONApprovalRequestSummary'
        type: array
    type: object
  messages.JSONApprovalResponse:
    properties:
      approve:
        type: boolean
    required:
    - approve
    type: object
  messages.JSONAttributes:
    additionalProperties:
      $ref: '#/definitions/messages.JSONAttributesDetails'
    type: object
  messages.JSONAttributesDetails:
    additionalProperties:
      type: string
    type: object
  messages.JSONBasicStatsResult:
    properties:
      aggregates:
        items:
          items:
            type: number
          type: array
        type: array
      count:
        example: 2645
        type: integer
    type: object
  messages.JSONBusinessRules:
    properties:
      businessrules:
        example:
        - 01 Attribute (Party)PartyB (Attribute)aantal cannot occur in the query
        - 03 Attribute (Party)PartyB (Attribute)einddatum can not occur as a filter with operator==
        items:
          type: string
        type: array
    type: object
  messages.JSONNewQuery:
    properties:
      metadata:
        $ref: '#/definitions/coordinator.NewQueryMetadata'
      query:
        $ref: '#/definitions/types.Query'
    required:
    - query
    type: object
  messages.JSONPPAVersion:
    properties:
      interface_backend:
        $ref: '#/definitions/messages.JSONVersion'
      mpc_compute_node:
        $ref: '#/definitions/messages.JSONVersion'
    type: object
  messages.JSONProtocolResult:
    properties:
      abortMessage:
        type: string
      destinationNodes:
        items:
          type: string
        type: array
      queryID:
        example: 86958258745911c77fbcf1519c6fb2dec4691c3b92bb9e71df696b6d5a8ab1cd
        type: string
      result:
        $ref: '#/definitions/messages.JSONBasicStatsResult'
      resultHash:
        example: 8aa253608c981d8d9b3130e7f4d8a60c9b491d2d93a454475a505b91bb6f9b29
        type: string
      success:
        example: true
        type: boolean
    type: object
  messages.JSONQueries:
    properties:
      queries:
        example:
        - 1c3acdf18d618fc6d6f912fc64002dae4c7af65f3bfc3e6b12302ba6c9242a6c
        - aba74d4182d4fd3dc6d99b7e36a005c888c9cf1cd7281e42f9ec15dd0fc25a66
        items:
          type: string
        type: array
    type: object
  messages.JSONQueryDetails:
    properties:
      approved_entry:
        $ref: '#/definitions/coordinator.LogEntryQueryApproved'
      completed_entry:
        $ref: '#/definitions/coordinator.LogEntryQueryComplete'
      manual_review_entry:
        $ref: '#/definitions/coordinator.LogEntryQueryManualReview'
    type: object
  messages.JSONResultError:
    properties:
      error:
        type: string
    type: object
  messages.JSONResultNewQuery:
    properties:
      queryID:
        type: string
    type: object
  messages.JSONResultOK:
    properties:
      success:
        type: boolean
    type: object
  messages.JSONVersion:
    properties:
      build_date:
        type: string
      git_branch:
        type: string
      git_commit:
        type: string
    type: object
  types.DatabaseAttribute:
    properties:
      enumValues:
        description: |-
          In case of kind=="enum": a list of possible values
          In all other cases, a nil slice
        items:
          type: string
        type: array
      kind:
        description: The type; can be "int", "string", "enum"
        type: string
      name:
        description: The name of the attribute as it occurs in the database
        type: string
    type: object
  types.Query:
    properties:
      aggregates:
        example:
        - party2:attr2 == 1
        items:
          type: string
        type: array
      filters:
        example:
        - SUM(party1:attr1)
        items:
          type: string
        type: array
    required:
    - aggregates
    - filters
    type: object
  types.TaggedQuery:
    properties:
      aggregates:
        example:
        - party2:attr2 == 1
        items:
          type: string
        type: array
      filters:
        example:
        - SUM(party1:attr1)
        items:
          type: string
        type: array
      id:
        example: 86958258745911c77fbcf1519c6fb2dec4691c3b92bb9e71df696b6d5a8ab1cd
        type: string
    required:
    - aggregates
    - filters
    type: object
host: localhost:8080
info:
  contact: {}
  description: This is the Interface Backend API for the PPA project.
  title: PPA Interface Backend API
  version: "1.0"
paths:
  /approvals:
    get:
      description: Get a list of approval requests summaries
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/messages.JSONApprovalRequests'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get approval requests
      tags:
      - Approval
  /approvals/{queryID}:
    get:
      description: Get approval request
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/messages.JSONApprovalRequestBasicStats'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get approval request
      tags:
      - Approval
    post:
      consumes:
      - application/json
      description: Submit approval response
      parameters:
      - description: Query
        in: body
        name: query
        required: true
        schema:
          $ref: '#/definitions/messages.JSONApprovalResponse'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/messages.JSONResultOK'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Submit approval response
      tags:
      - Approval
  /attributes:
    get:
      description: Get the attributes from all participants from the smartcontract
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/messages.JSONAttributes'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get the attibutes from the smartcontract
      tags:
      - Root
  /auditlog:
    get:
      description: |-
        Get the auditlog

        This route returns an array with different kinds of LogEntries. Every object in the array contains the attributes "kind", "timestamp" and "contents". The contents depends on the "kind" attribute.

        The following kinds of LogEntries are supported:
        PresenceGUI = 1
        PresenceMPC = 2
        QueryApproved = 4
        QueryRejected = 8
        QueryUnaffordable = 16
        QueryComplete 32
      responses:
        "200":
          description: OK
          schema:
            items:
              allOf:
              - $ref: '#/definitions/coordinator.LogEntry'
              - properties:
                  contents1:
                    $ref: '#/definitions/coordinator.LogEntryPresenceGUI'
                  contents2:
                    $ref: '#/definitions/coordinator.LogEntryPresenceMPC'
                  contents4:
                    $ref: '#/definitions/coordinator.LogEntryQueryApproved'
                  contents8:
                    $ref: '#/definitions/coordinator.LogEntryQueryRejected'
                  contents16:
                    $ref: '#/definitions/coordinator.LogEntryQueryUnaffordable'
                  contents32:
                    $ref: '#/definitions/coordinator.LogEntryQueryComplete'
                type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get auditlog of the smartcontract
      tags:
      - Root
  /auditlog/{pageID}:
    get:
      description: |-
        Get a page of the auditlog

        This route returns an array with different kinds of LogEntries. Every object in the array contains the attributes "kind", "timestamp" and "contents". The contents depends on the "kind" attribute.

        The following kinds of LogEntries are supported:
        PresenceGUI = 1
        PresenceMPC = 2
        QueryApproved = 4
        QueryRejected = 8
        QueryUnaffordable = 16
        QueryComplete 32
      parameters:
      - description: Page ID
        in: path
        name: pageID
        required: true
        type: integer
      responses:
        "200":
          description: OK
          schema:
            items:
              allOf:
              - $ref: '#/definitions/coordinator.LogEntry'
              - properties:
                  contents1:
                    $ref: '#/definitions/coordinator.LogEntryPresenceGUI'
                  contents2:
                    $ref: '#/definitions/coordinator.LogEntryPresenceMPC'
                  contents4:
                    $ref: '#/definitions/coordinator.LogEntryQueryApproved'
                  contents8:
                    $ref: '#/definitions/coordinator.LogEntryQueryRejected'
                  contents16:
                    $ref: '#/definitions/coordinator.LogEntryQueryUnaffordable'
                  contents32:
                    $ref: '#/definitions/coordinator.LogEntryQueryComplete'
                type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get auditlog page of the smartcontract
      tags:
      - Root
  /businessrules:
    get:
      description: Get active businessrules
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/messages.JSONBusinessRules'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get businessrules
      tags:
      - Root
  /eventstream:
    get:
      description: Connect to a websocket which streams real-time auditlog events
      responses:
        "101":
          description: Switching Protocols - Switch to websocket
          schema:
            type: string
        "400":
          description: Bad Request - Not a websocket connection
          schema:
            type: string
      summary: '[Websocket] Connect to event stream'
      tags:
      - Root
  /queries:
    get:
      description: Get all query identifiers in the smartcontract
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/messages.JSONQueries'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get all query ID's
      tags:
      - Queries
    post:
      consumes:
      - application/json
      description: Create a new query on the smart contract
      parameters:
      - description: Query
        in: body
        name: query
        required: true
        schema:
          $ref: '#/definitions/messages.JSONNewQuery'
      produces:
      - application/json
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/messages.JSONResultNewQuery'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Create new query
      tags:
      - Queries
  /queries/{queryID}:
    get:
      description: Get query details
      parameters:
      - description: Query ID
        in: path
        name: queryID
        required: true
        type: string
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/messages.JSONQueryDetails'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get query
      tags:
      - Queries
  /queries/{queryID}/auditlog:
    get:
      description: |-
        Get the auditlog of the query

        This route returns an array with different kinds of LogEntries. Every object in the array contains the attributes "kind", "timestamp" and "contents". The contents depends on the "kind" attribute.

        The following kinds of LogEntries are supported:
        QueryApproved = 4
        QueryRejected = 8
        QueryUnaffordable = 16
        QueryComplete 32
      parameters:
      - description: Query ID
        in: path
        name: queryID
        required: true
        type: string
      responses:
        "200":
          description: OK
          schema:
            items:
              allOf:
              - $ref: '#/definitions/coordinator.LogEntry'
              - properties:
                  contents4:
                    $ref: '#/definitions/coordinator.LogEntryQueryApproved'
                  contents8:
                    $ref: '#/definitions/coordinator.LogEntryQueryRejected'
                  contents16:
                    $ref: '#/definitions/coordinator.LogEntryQueryUnaffordable'
                  contents32:
                    $ref: '#/definitions/coordinator.LogEntryQueryComplete'
                type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get query auditlog
      tags:
      - Queries
  /queries/{queryID}/auditlog/{pageID}:
    get:
      description: |-
        Get a page of the auditlog of the query

        This route returns an array with different kinds of LogEntries. Every object in the array contains the attributes "kind", "timestamp" and "contents". The contents depends on the "kind" attribute.

        The following kinds of LogEntries are supported:
        QueryApproved = 4
        QueryRejected = 8
        QueryUnaffordable = 16
        QueryComplete 32
      parameters:
      - description: Query ID
        in: path
        name: queryID
        required: true
        type: string
      - description: Page ID
        in: path
        name: pageID
        required: true
        type: integer
      responses:
        "200":
          description: OK
          schema:
            items:
              allOf:
              - $ref: '#/definitions/coordinator.LogEntry'
              - properties:
                  contents4:
                    $ref: '#/definitions/coordinator.LogEntryQueryApproved'
                  contents8:
                    $ref: '#/definitions/coordinator.LogEntryQueryRejected'
                  contents16:
                    $ref: '#/definitions/coordinator.LogEntryQueryUnaffordable'
                  contents32:
                    $ref: '#/definitions/coordinator.LogEntryQueryComplete'
                type: object
            type: array
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get query auditlog page
      tags:
      - Queries
  /queries/{queryID}/result:
    get:
      description: Get the result of the query
      parameters:
      - description: Query ID
        in: path
        name: queryID
        required: true
        type: string
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/messages.JSONProtocolResult'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get query result
      tags:
      - Queries
  /version:
    get:
      description: Get version of the interface backend and mpc compute node
      responses:
        "200":
          description: OK
          schema:
            $ref: '#/definitions/messages.JSONPPAVersion'
        "400":
          description: Bad Request
          schema:
            $ref: '#/definitions/messages.JSONResultError'
      summary: Get version
      tags:
      - Root
swagger: "2.0"
