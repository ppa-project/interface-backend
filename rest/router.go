// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rest

import (
	"encoding/json"
	"net/http"
	"regexp"
	"strconv"

	"ci.tno.nl/gitlab/ppa-project/pkg/http/server"

	"ci.tno.nl/gitlab/ppa-project/interface-backend/rest/messages"
	"ci.tno.nl/gitlab/ppa-project/interface-backend/service"
	"ci.tno.nl/gitlab/ppa-project/interface-backend/version"

	_ "ci.tno.nl/gitlab/ppa-project/interface-backend/docs"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

type RESTController struct {
	service *service.Service
	ww      *WebsocketWriter
}

// NewRESTController Constructor for RESTController
func NewRESTController(service *service.Service) *RESTController {
	ww := NewWebsocketWriter()
	return &RESTController{service: service, ww: ww}
}

// @title PPA Interface Backend API
// @version 1.0
// @description This is the Interface Backend API for the PPA project.

// @host localhost:8080
// @BasePath /

// GetRouter get main router
func (rc *RESTController) GetRouter() http.Handler {
	if !viper.GetBool("debug") {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.New()

	// Recovery middleware recovers from any panics and writes a 500 if there was one.
	r.Use(gin.Recovery())

	if viper.GetBool("debug") {
		r.Use(cors.Default())
	}

	if viper.GetBool("dev") {
		url := ginSwagger.URL("http://localhost:8888/swagger/doc.json") // The url pointing to API definition
		r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
	}

	r.Use(server.RouteMetrics())
	r.Use(server.RouteLogIncoming())

	r.GET("/version", rc.GetVersion)

	// Routes consist of a path and a handler function.
	r.StaticFS("/static/", http.Dir("static"))

	r.GET("/businessrules", rc.GetBusinessRules)

	r.GET("/eventstream", rc.GetEventStream)

	r.GET("/auditlog", rc.GetAuditLog)
	r.GET("/auditlog/:pageID", rc.GetAuditLogPage)

	r.GET("/attributes", rc.GetAttributes)

	r.Use(ValidateParams())

	// smartcontract
	queriesGroup := r.Group("/queries")
	rc.addSmartcontractHandlers(queriesGroup)

	// approval
	approvalGroup := r.Group("/approvals")
	rc.addApprovalHandlers(approvalGroup)

	return r
}

// GetVersion godoc
// @Summary Get version
// @Description Get version of the interface backend and mpc compute node
// @Tags Root
// @Success 200 {object} messages.JSONPPAVersion
// @Success 400 {object} messages.JSONResultError
// @Router /version [get]
func (rc *RESTController) GetVersion(c *gin.Context) {
	uiVersion := messages.JSONVersion{
		GitBranch: version.GitBranch,
		GitCommit: version.GitCommit,
		BuildDate: version.BuildDate,
	}

	version := messages.JSONPPAVersion{
		InterfaceBackend: &uiVersion,
	}

	result, err := rc.service.FetchMPCVersion()
	if err != nil {
		log.Debug().Err(err).Msg("Unable to fetch MPC version")
		c.JSON(http.StatusOK, version)
		return
	}

	var mpcVersion messages.JSONVersion
	err = json.Unmarshal(result, &mpcVersion)
	if err != nil {
		log.Debug().Err(err).Msg("Unable to unmarshal MPC version")
		c.JSON(http.StatusOK, version)
		return
	}
	version.MPCComputeNode = &mpcVersion

	c.JSON(http.StatusOK, version)
}

// GetBusinessRules godoc
// @Summary Get businessrules
// @Description Get active businessrules
// @Tags Root
// @Success 200 {object} messages.JSONBusinessRules
// @Success 400 {object} messages.JSONResultError
// @Router /businessrules [get]
func (rc *RESTController) GetBusinessRules(c *gin.Context) {
	c.JSON(http.StatusOK, &gin.H{
		"businessrules": rc.service.GetBusinessRules(),
	})
}

// GetEventStream godoc
// @Summary [Websocket] Connect to event stream
// @Description Connect to a websocket which streams real-time auditlog events
// @Tags Root
// @Success 101 {string} string "Switching Protocols - Switch to websocket"
// @Failure 400 {string} string "Bad Request - Not a websocket connection"
// @Router /eventstream [get]
func (rc *RESTController) GetEventStream(c *gin.Context) {
	rc.eventStreamHandler(c.Writer, c.Request)
}

// GetAuditLog godoc
// @Summary Get auditlog of the smartcontract
// @Description Get the auditlog
// @Description
// @Description This route returns an array with different kinds of LogEntries. Every object in the array contains the attributes "kind", "timestamp" and "contents". The contents depends on the "kind" attribute.
// @Description
// @Description The following kinds of LogEntries are supported:
// @Description PresenceGUI = 1
// @Description PresenceMPC = 2
// @Description QueryApproved = 4
// @Description QueryRejected = 8
// @Description QueryUnaffordable = 16
// @Description QueryComplete 32
// @Tags Root
// @Success 200 {array} coordinator.LogEntry{contents1=coordinator.LogEntryPresenceGUI,contents2=coordinator.LogEntryPresenceMPC,contents4=coordinator.LogEntryQueryApproved,contents8=coordinator.LogEntryQueryRejected,contents16=coordinator.LogEntryQueryUnaffordable,contents32=coordinator.LogEntryQueryComplete}
// @Success 400 {object} messages.JSONResultError
// @Router /auditlog [get]
func (rc *RESTController) GetAuditLog(c *gin.Context) {
	result := rc.service.GetAuditLogs()
	c.JSON(http.StatusOK, result)
}

// GetAuditLogPage godoc
// @Summary Get auditlog page of the smartcontract
// @Description Get a page of the auditlog
// @Description
// @Description This route returns an array with different kinds of LogEntries. Every object in the array contains the attributes "kind", "timestamp" and "contents". The contents depends on the "kind" attribute.
// @Description
// @Description The following kinds of LogEntries are supported:
// @Description PresenceGUI = 1
// @Description PresenceMPC = 2
// @Description QueryApproved = 4
// @Description QueryRejected = 8
// @Description QueryUnaffordable = 16
// @Description QueryComplete 32
// @Tags Root
// @Param pageID path int true "Page ID"
// @Success 200 {array} coordinator.LogEntry{contents1=coordinator.LogEntryPresenceGUI,contents2=coordinator.LogEntryPresenceMPC,contents4=coordinator.LogEntryQueryApproved,contents8=coordinator.LogEntryQueryRejected,contents16=coordinator.LogEntryQueryUnaffordable,contents32=coordinator.LogEntryQueryComplete}
// @Success 400 {object} messages.JSONResultError
// @Router /auditlog/{pageID} [get]
func (rc *RESTController) GetAuditLogPage(c *gin.Context) {
	r, _ := regexp.Compile("^[0-9]+$")
	pageID := c.Param("pageID")
	if !r.MatchString(pageID) {
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: "pageID is invalid"})
		return
	}
	page, err := strconv.Atoi(pageID)
	if err != nil {
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: "pageID is not an integer"})
		return
	}

	result := rc.service.GetAuditLogsWithPage(page)
	c.JSON(http.StatusOK, result)
}

// GetAttributes godoc
// @Summary Get the attibutes from the smartcontract
// @Description Get the attributes from all participants from the smartcontract
// @Tags Root
// @Success 200 {object} messages.JSONAttributes
// @Success 400 {object} messages.JSONResultError
// @Router /attributes [get]
func (rc *RESTController) GetAttributes(c *gin.Context) {
	participants, err := rc.service.GetOrganizations()
	if err != nil {
		log.Error().Err(err).Msg("Unable to fetch organization list from coordinator")
		c.JSON(http.StatusInternalServerError, messages.JSONResultError{Error: "Failed to retrieve list of organizations"})
		return
	}

	response := gin.H{}
	for _, participant := range participants {
		responsepart := gin.H{}
		result, err := rc.service.GetAttributes(participant)
		if err == nil {
			for _, attr := range result {
				if attr.Kind == "enum" {
					responsepart[attr.Name] = attr.EnumValues
				} else {
					responsepart[attr.Name] = attr.Kind
				}
			}
		}
		response[participant] = responsepart
	}
	c.JSON(http.StatusOK, response)
}
