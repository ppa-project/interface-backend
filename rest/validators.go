// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rest

import (
	"net/http"
	"regexp"

	"ci.tno.nl/gitlab/ppa-project/pkg/http/server"
	"github.com/gin-gonic/gin"
)

func ValidateParams() gin.HandlerFunc {
	return func(c *gin.Context) {
		r, _ := regexp.Compile("^[a-fA-F0-9]{64}$")
		queryID := c.Param("queryID")
		if queryID != "" && !r.MatchString(queryID) {
			server.Error(c, http.StatusBadRequest, "queryID is invalid")
			return
		}
		c.Next()
	}
}
