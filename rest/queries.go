// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rest

import (
	"encoding/json"
	"net/http"
	"regexp"
	"strconv"

	"github.com/rs/zerolog/log"

	_ "ci.tno.nl/gitlab/ppa-project/interface-backend/docs"
	"ci.tno.nl/gitlab/ppa-project/interface-backend/rest/messages"
	"github.com/gin-gonic/gin"
)

func (rc *RESTController) addSmartcontractHandlers(r *gin.RouterGroup) {
	r.POST("", rc.NewQuery)
	r.GET("", rc.GetQueries)
	r.GET("/:queryID", rc.GetQuery)
	r.GET("/:queryID/result", rc.GetQueryResult)
	r.GET("/:queryID/auditlog", rc.GetQueryAuditLog)
	r.GET("/:queryID/auditlog/:pageID", rc.GetQueryAuditLogPage)
}

// NewQuery godoc
// @Summary Create new query
// @Description Create a new query on the smart contract
// @Tags Queries
// @Accept json
// @Produce json
// @Param query body messages.JSONNewQuery true "Query"
// @Success 200 {object} messages.JSONResultNewQuery
// @Success 400 {object} messages.JSONResultError
// @Router /queries [post]
func (rc *RESTController) NewQuery(c *gin.Context) {
	var newQuery messages.JSONNewQuery

	if err := c.ShouldBindJSON(&newQuery); err != nil {
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: err.Error()})
		return
	}

	queryID, err := rc.service.ProcessNewQuery(newQuery.Query, newQuery.Metadata)
	if err != nil {
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: err.Error()})
		return
	}

	log.Info().Str("queryID", queryID).Interface("query", newQuery).Msg("Succesfully submitted new query")

	c.JSON(http.StatusOK, messages.JSONResultNewQuery{QueryID: queryID})
}

// GetQueries godoc
// @Summary Get all query ID's
// @Description Get all query identifiers in the smartcontract
// @Tags Queries
// @Success 200 {object} messages.JSONQueries
// @Success 400 {object} messages.JSONResultError
// @Router /queries [get]
func (rc *RESTController) GetQueries(c *gin.Context) {
	c.JSON(http.StatusOK, messages.JSONQueries{
		Queries: rc.service.GetQueries(),
	})
}

// GetQuery godoc
// @Summary Get query
// @Description Get query details
// @Tags Queries
// @Success 200 {object} messages.JSONQueryDetails
// @Success 400 {object} messages.JSONResultError
// @Param queryID path string true "Query ID"
// @Router /queries/{queryID} [get]
func (rc *RESTController) GetQuery(c *gin.Context) {
	queryDetails, err := rc.service.GetQueryDetails(c.Param("queryID"))
	if err != nil {
		log.Debug().Err(err).Msg("Failed to fetch query details")
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: "Failed to fetch query details"})
		return
	}
	c.JSON(http.StatusOK, &gin.H{
		"approved_entry":      queryDetails.LogEntryQueryApproved,
		"completed_entry":     queryDetails.LogEntryQueryComplete,
		"manual_review_entry": queryDetails.LogEntryQueryManualReview,
	})
}

// GetQueryResult godoc
// @Summary Get query result
// @Description Get the result of the query
// @Tags Queries
// @Success 200 {object} messages.JSONProtocolResult
// @Success 400 {object} messages.JSONResultError
// @Param queryID path string true "Query ID"
// @Router /queries/{queryID}/result [get]
func (rc *RESTController) GetQueryResult(c *gin.Context) {
	result, err := rc.service.FetchQueryResult(c.Param("queryID"))
	if err != nil {
		log.Error().Err(err).Msg("Query result is not available yet")
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: "Query result is not available yet"})
		return
	}

	log.Debug().Str("result", string(result)).Msg("Got result from MPC Node")

	var protocolResult messages.JSONProtocolResult
	err = json.Unmarshal(result, &protocolResult)
	if err != nil {
		log.Error().Err(err).Msg("Malformed query result")
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: "Failed to fetch query result"})
		return
	}

	c.JSON(http.StatusOK, protocolResult)
}

// GetQueryAuditLog godoc
// @Summary Get query auditlog
// @Description Get the auditlog of the query
// @Description
// @Description This route returns an array with different kinds of LogEntries. Every object in the array contains the attributes "kind", "timestamp" and "contents". The contents depends on the "kind" attribute.
// @Description
// @Description The following kinds of LogEntries are supported:
// @Description QueryApproved = 4
// @Description QueryRejected = 8
// @Description QueryUnaffordable = 16
// @Description QueryComplete 32
// @Tags Queries
// @Param queryID path string true "Query ID"
// @Success 200 {array} coordinator.LogEntry{contents4=coordinator.LogEntryQueryApproved,contents8=coordinator.LogEntryQueryRejected,contents16=coordinator.LogEntryQueryUnaffordable,contents32=coordinator.LogEntryQueryComplete}
// @Success 400 {object} messages.JSONResultError
// @Router /queries/{queryID}/auditlog [get]
func (rc *RESTController) GetQueryAuditLog(c *gin.Context) {
	result := rc.service.GetAuditLogsWithQueryID(c.Param("queryID"))
	c.JSON(http.StatusOK, result)
}

// GetQueryAuditLogPage godoc
// @Summary Get query auditlog page
// @Description Get a page of the auditlog of the query
// @Description
// @Description This route returns an array with different kinds of LogEntries. Every object in the array contains the attributes "kind", "timestamp" and "contents". The contents depends on the "kind" attribute.
// @Description
// @Description The following kinds of LogEntries are supported:
// @Description QueryApproved = 4
// @Description QueryRejected = 8
// @Description QueryUnaffordable = 16
// @Description QueryComplete 32
// @Tags Queries
// @Param queryID path string true "Query ID"
// @Param pageID path int true "Page ID"
// @Success 200 {array} coordinator.LogEntry{contents4=coordinator.LogEntryQueryApproved,contents8=coordinator.LogEntryQueryRejected,contents16=coordinator.LogEntryQueryUnaffordable,contents32=coordinator.LogEntryQueryComplete}
// @Success 400 {object} messages.JSONResultError
// @Router /queries/{queryID}/auditlog/{pageID} [get]
func (rc *RESTController) GetQueryAuditLogPage(c *gin.Context) {
	r, _ := regexp.Compile("^[0-9]+$")
	pageID := c.Param("pageID")
	if !r.MatchString(pageID) {
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: "pageID is invalid"})
		return
	}
	page, err := strconv.Atoi(pageID)
	if err != nil {
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: "pageID is not an integer"})
		return
	}

	result := rc.service.GetAuditLogsPageWithQueryID(c.Param("queryID"), page)
	c.JSON(http.StatusOK, result)
}
