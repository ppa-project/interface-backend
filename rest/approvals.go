// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rest

import (
	"encoding/json"
	"net/http"
	"sort"

	"ci.tno.nl/gitlab/ppa-project/interface-backend/rest/messages"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

func (rc *RESTController) addApprovalHandlers(r *gin.RouterGroup) {
	r.GET("", rc.GetApprovalRequests)
	r.GET("/:queryID", rc.GetApprovalRequest)
	r.POST("/:queryID", rc.PostApprovalResponse)
}

var processedApprovalRequests = make(map[string]bool)

// GetApprovalRequests godoc
// @Summary Get approval requests
// @Description Get a list of approval requests summaries
// @Tags Approval
// @Success 200 {object} messages.JSONApprovalRequests
// @Success 400 {object} messages.JSONResultError
// @Router /approvals [get]
func (rc *RESTController) GetApprovalRequests(c *gin.Context) {
	result, err := rc.service.FetchApprovalRequests()
	if err != nil {
		log.Error().Err(err).Msg("Failed to fetch query result")
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: "Failed to fetch approval requests"})
		return
	}
	var approvalRequests messages.JSONApprovalRequests
	err = json.Unmarshal(result, &approvalRequests)
	if err != nil {
		log.Error().Err(err).Msg("Failed to unmarshal response")
		c.Data(http.StatusBadRequest, "application/json", result)
		return
	}

	for i, req := range approvalRequests.ApprovalRequests {
		manualReviewLogEntries := rc.service.GetManualReviewLogEntries()
		if entry, ok := manualReviewLogEntries[req.QueryID]; ok {
			approvalRequests.ApprovalRequests[i].CreationTimestamp = entry.Timestamp.String()
		}
		completedLogEntries := rc.service.GetQueryCompleteLogEntries()
		if entry, ok := completedLogEntries[req.QueryID]; ok {
			approvalRequests.ApprovalRequests[i].CompletedTimestamp = entry.Timestamp.String()
		}

		if approved, ok := processedApprovalRequests[req.QueryID]; ok {
			if approved {
				approvalRequests.ApprovalRequests[i].Status = "Approved"
			} else {
				approvalRequests.ApprovalRequests[i].Status = "Rejected"
			}
		} else if approvalRequests.ApprovalRequests[i].CompletedTimestamp != "" {
			approvalRequests.ApprovalRequests[i].Status = "Unknown"
		}
	}

	sort.Slice(approvalRequests.ApprovalRequests, func(i, j int) bool {
		a := approvalRequests.ApprovalRequests[i]
		b := approvalRequests.ApprovalRequests[j]
		if a.CreationTimestamp == "" {
			return true
		}
		if b.CreationTimestamp == "" {
			return false
		}
		return a.CreationTimestamp > b.CreationTimestamp
	})

	c.JSON(http.StatusOK, approvalRequests)
}

// GetApprovalRequest godoc
// @Summary Get approval request
// @Description Get approval request
// @Tags Approval
// @Success 200 {object} messages.JSONApprovalRequestBasicStats
// @Success 400 {object} messages.JSONResultError
// @Router /approvals/{queryID} [get]
func (rc *RESTController) GetApprovalRequest(c *gin.Context) {
	result, err := rc.service.FetchApprovalRequest(c.Param("queryID"))
	if err != nil {
		log.Error().Err(err).Msg("Failed to fetch query result")
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: "Failed to fetch approval requests"})
		return
	}
	// TODO: generalize for different protocols
	var approvalRequests messages.JSONApprovalRequestBasicStats
	err = json.Unmarshal(result, &approvalRequests)
	if err != nil {
		log.Error().Err(err).Msg("Failed to unmarshal response")
		c.Data(http.StatusBadRequest, "application/json", result)
		return
	}
	c.JSON(http.StatusOK, approvalRequests)
}

// PostApprovalResponse godoc
// @Summary Submit approval response
// @Description Submit approval response
// @Tags Approval
// @Accept json
// @Produce json
// @Param query body messages.JSONApprovalResponse true "Query"
// @Success 200 {object} messages.JSONResultOK
// @Success 400 {object} messages.JSONResultError
// @Router /approvals/{queryID} [post]
func (rc *RESTController) PostApprovalResponse(c *gin.Context) {
	var approvalResponse messages.JSONApprovalResponse

	if err := c.ShouldBindJSON(&approvalResponse); err != nil {
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: err.Error()})
		return
	}

	result, err := rc.service.SubmitApprovalResponse(c.Param("queryID"), *approvalResponse.Approve)
	if err != nil {
		log.Error().Err(err).Msg("Failed to fetch query result")
		c.JSON(http.StatusBadRequest, messages.JSONResultError{Error: "Failed to submit approval response"})
		return
	}
	var resultOK messages.JSONResultOK
	err = json.Unmarshal(result, &resultOK)
	if err != nil {
		log.Error().Err(err).Msg("Failed to unmarshal response")
		c.Data(http.StatusBadRequest, "application/json", result)
		return
	}

	processedApprovalRequests[c.Param("queryID")] = *approvalResponse.Approve

	c.JSON(http.StatusOK, resultOK)

}
