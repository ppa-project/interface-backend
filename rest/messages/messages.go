// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package messages

import (
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
)

type JSONResultOK struct {
	Success bool `json:"success"`
}

type JSONResultError struct {
	Error string `json:"error"`
}

type JSONResultNewQuery struct {
	QueryID string `json:"queryID"`
}

type JSONNewQuery struct {
	Query    types.Query                  `json:"query" binding:"required"`
	Metadata coordinator.NewQueryMetadata `json:"metadata,omitempty"`
}

type JSONProtocolResult struct {
	QueryID          string               `json:"queryID" example:"86958258745911c77fbcf1519c6fb2dec4691c3b92bb9e71df696b6d5a8ab1cd"`
	IsSuccess        bool                 `json:"success" example:"true"`
	AbortMessage     string               `json:"abortMessage" example:""`
	Result           JSONBasicStatsResult `json:"result"`
	ResultHash       string               `json:"resultHash" example:"8aa253608c981d8d9b3130e7f4d8a60c9b491d2d93a454475a505b91bb6f9b29"`
	DestinationNodes []string             `json:"destinationNodes"`
}

type JSONBasicStatsResult struct {
	Count      int64       `json:"count" example:"2645"`
	Aggregates [][]float64 `json:"aggregates"`
}

type JSONQueries struct {
	Queries []string `json:"queries" example:"1c3acdf18d618fc6d6f912fc64002dae4c7af65f3bfc3e6b12302ba6c9242a6c,aba74d4182d4fd3dc6d99b7e36a005c888c9cf1cd7281e42f9ec15dd0fc25a66"`
}

type JSONQueryDetails struct {
	ApprovedEntry     coordinator.LogEntryQueryApproved     `json:"approved_entry"`
	CompletedEntry    coordinator.LogEntryQueryComplete     `json:"completed_entry"`
	ManualReviewEntry coordinator.LogEntryQueryManualReview `json:"manual_review_entry"`
}

type JSONAttributes map[string]JSONAttributesDetails
type JSONAttributesDetails map[string]string

type JSONBusinessRules struct {
	BusinessRules []string `json:"businessrules" example:"01 Attribute (Party)PartyB (Attribute)aantal cannot occur in the query,03 Attribute (Party)PartyB (Attribute)einddatum can not occur as a filter with operator=="`
}

// Approval
type JSONApprovalRequests struct {
	ApprovalRequests []JSONApprovalRequestSummary `json:"approvalRequests"`
}

type JSONApprovalRequestSummary struct {
	QueryID            string `json:"queryID"`
	Summary            string `json:"summary"`
	Status             string `json:"status"`
	CreationTimestamp  string `json:"creation_timestamp"`
	CompletedTimestamp string `json:"completed_timestamp"`
}

type JSONApprovalRequestBasicStats struct {
	Query      *types.TaggedQuery `json:"query"`
	Count      int64              `json:"count"`
	Aggregates []float64          `json:"aggregates"`
}

type JSONApprovalResponse struct {
	Approve *bool `json:"approve" swaggertype:"boolean" binding:"required"`
}

type JSONVersion struct {
	GitBranch string `json:"git_branch"`
	GitCommit string `json:"git_commit"`
	BuildDate string `json:"build_date"`
}

type JSONPPAVersion struct {
	InterfaceBackend *JSONVersion `json:"interface_backend"`
	MPCComputeNode   *JSONVersion `json:"mpc_compute_node"`
}
