// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package rest

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"sync"

	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"github.com/gorilla/websocket"
	"github.com/rs/zerolog/log"
)

func (rc *RESTController) eventStreamHandler(w http.ResponseWriter, r *http.Request) {
	var wsupgrader = websocket.Upgrader{
		ReadBufferSize:  2048,
		WriteBufferSize: 2048,
	}

	conn, err := wsupgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Warn().Err(err).Msg("Failed to upgrade websocket")
		return
	}

	rc.registerWSConnection(conn)
}

func (rc *RESTController) NewAuditLogEntry(entry coordinator.LogEntry) {
	rc.ww.mu.Lock()
	defer rc.ww.mu.Unlock()
	// TODO: filter logs for output
	msg, err := json.Marshal(entry)
	if err != nil {
		log.Warn().Err(err).Msg("Failed to Marshal LogEntry")
	}
	for _, ch := range rc.ww.chans {
		*ch <- msg
	}
}

// WebsocketWriter manages active websocket connections
type WebsocketWriter struct {
	mu    sync.Mutex
	chans map[int]*chan []byte
}

// NewWebsocketWriter constructs a new WebsocketWriter
func NewWebsocketWriter() *WebsocketWriter {
	return &WebsocketWriter{chans: make(map[int]*chan []byte)}
}

func (rc *RESTController) registerWSConnection(conn *websocket.Conn) {
	rc.ww.mu.Lock()
	defer rc.ww.mu.Unlock()
	id := -1
	exists := true
	for exists {
		id = rand.Int()
		_, exists = rc.ww.chans[id]
	}
	updateChan := make(chan []byte, 5)
	rc.ww.chans[id] = &updateChan

	go func() {
		mainloop := true
		for mainloop {
			select {
			case msg := <-updateChan:
				err := conn.WriteMessage(websocket.TextMessage, msg)
				if err != nil {
					conn.Close()
					rc.ww.mu.Lock()
					defer rc.ww.mu.Unlock()
					delete(rc.ww.chans, id)
					mainloop = false
				}
			}
		}
	}()

	rc.pushTrailingEvents(&updateChan)
}

func (rc *RESTController) pushTrailingEvents(updateChan *chan []byte) {
	for _, logEntry := range rc.service.GetTrailingAuditLogs(10, coordinator.Any) {
		b, err := json.Marshal(&logEntry)
		if err == nil {
			*updateChan <- b
		}
	}
}
