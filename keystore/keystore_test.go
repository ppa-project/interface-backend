// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package keystore

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

const FILENAME = "_testkey"

func TestMain(m *testing.M) {
	zerolog.DurationFieldInteger = true
	zerolog.SetGlobalLevel(zerolog.DebugLevel)

	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, NoColor: true})

	viper.SetDefault("ethereum.keyfile", FILENAME)

	code := m.Run()
	os.Remove(FILENAME) // Just to be sure
	os.Exit(code)
}

func TestEthereumSecretKey(t *testing.T) {
	assert := assert.New(t)

	assert.False(fileExists(FILENAME), "Test cache file existed at start of test")

	// Generate a new key
	sk := GetEthereumPrivateKey()
	assert.False(len(sk) == 0, "Could not make a new key")
	assert.True(fileExists(FILENAME), "Did not save new key to cache")

	fileContents, _ := ioutil.ReadFile(FILENAME)

	// Load a stored key
	sk2 := GetEthereumPrivateKey()
	assert.False(len(sk2) == 0, "Could not get cached key")
	assert.Equal(sk, sk2, "Made a new key while a cached one existed")

	fileContents2, _ := ioutil.ReadFile(FILENAME)
	assert.Equal(fileContents, fileContents2, "Cache file changed while reading it")

	// Fail to load a corrupted key
	assert.Nil(os.Remove(FILENAME), "Couldn't delete test cache file")
	ioutil.WriteFile(FILENAME, []byte("Hello Ethereum"), 0600)

	sk3 := GetEthereumPrivateKey()
	assert.False(len(sk3) == 0, "Failed in the face of a corrupted key cache")
	fileContents3, _ := ioutil.ReadFile(FILENAME)
	assert.NotEqual(fileContents3, []byte("Hello Ethereum"), "Did not replace corrupted key file")

	assert.Nil(os.Remove(FILENAME), "Couldn't delete test cache file")
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
