// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package keystore

import (
	"encoding/hex"
	"errors"
	"fmt"
	"io/ioutil"

	"github.com/ethereum/go-ethereum/crypto"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

// getEthereumPrivateKey returns a new or existing PK for this node to use.
// If one is given in the configuration (via Viper as ethereum.privatekey),
// that is used. Otherwise, we check if a key is cached on disk. If not, we
// generate a new one, and attempt to write it to disk.
func GetEthereumPrivateKey() string {
	// Try to load the key from config
	viperKey := viper.GetString("ethereum.privatekey")

	_, err := crypto.HexToECDSA(viperKey)
	if err == nil {
		return viperKey
	}

	log.Debug().
		AnErr("crypto.HexToECDSA-error", err).
		Str("ethereum.privatekey", viperKey).
		Msg("No valid Ethereum private key configured via Viper")

	// Try to load the key from storage
	diskKey := getEthereumKeyFromStorage()
	if len(diskKey) != 0 {
		return diskKey
	}

	// Make a new key and try to store it
	newKey, err := makeNewKey()
	if err != nil {
		// This should not happen outside of truly pathological situations
		// (lack of randomness? No memory left?), so we abort
		log.Fatal().
			AnErr("crypto.GenerateKey-error", err).
			Msg("Can not generate a new Ethereum private key")
	}
	err = storeKey(newKey)
	if err != nil {
		log.Err(err).
			Msg("Can not store newly generated Ethereum private key")
	}
	return newKey
}

func getEthereumKeyFromStorage() string {
	keyfile := viper.GetString("ethereum.keyfile")
	if keyfile == "" {
		log.Info().Msg("Can't load stored Ethereum private key: no Ethereum key file given in config")
		return ""
	}
	keybytes, err := ioutil.ReadFile(keyfile)
	if err != nil {
		log.Warn().
			AnErr("ioutil.ReadFile-error", err).
			Str("filename", keyfile).
			Msg("Can't load stored Ethereum private key: Ethereum private key file can't be read")
		return ""
	}
	_, err = crypto.HexToECDSA(string(keybytes))
	if err != nil {
		log.Warn().
			AnErr("crypto.HexToECDSA-error", err).
			Str("ethereum.privatekey", string(keybytes)).
			Msg("Stored Ethereum private key is not a valid ECDSA private key")
		return ""
	}

	return string(keybytes)
}

func makeNewKey() (string, error) {
	key, err := crypto.GenerateKey()
	if err != nil {
		return "", err
	}
	return hex.EncodeToString(crypto.FromECDSA(key)), nil
}

func storeKey(key string) error {
	keyfile := viper.GetString("ethereum.keyfile")
	if keyfile == "" {
		return errors.New("Can't load stored Ethereum private key: no Ethereum key file given in config")
	}

	err := ioutil.WriteFile(keyfile, []byte(key), 0600)
	if err != nil {
		return fmt.Errorf("Can't write to file `%s`: %v", keyfile, err)
	}
	return nil
}
