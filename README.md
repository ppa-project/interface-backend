# Interface Backend

[API Swagger Documentation](docs/swagger.json)

## Generate swagger files

```sh
$ go get -u github.com/swaggo/swag/cmd/swag
$ swag init -g rest/router.go --parseDependency
```

## Run server

```sh
$ go build
$ ./interface-backend serve config.toml [-v]
```
