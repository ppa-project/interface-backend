// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package transport provides the layer that translates MPC messages to HTTP requests.
package transport

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strconv"
	"sync"
	"time"

	"ci.tno.nl/gitlab/ppa-project/interface-backend/rest/messages"
	h "ci.tno.nl/gitlab/ppa-project/pkg/http"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

// MessageSender generalizes the MessageSender methods
type MessageSender interface {
	FetchMPCVersion() ([]byte, error)
	FetchQueryResult(queryID string) ([]byte, error)
	FetchApprovalRequests() ([]byte, error)
	FetchApprovalRequest(queryID string) ([]byte, error)
	SubmitApprovalResponse(queryID string, approve bool) ([]byte, error)
}

// MessageSenderHTTP implements the MessageSender interface
type MessageSenderHTTP struct {
	sync.Mutex
	config        h.Config
	mpcNodeConfig h.Config
	client        *http.Client
}

// NewMessageSenderHTTP Constructor for MessageSenderHTTP
func NewMessageSenderHTTP(config h.Config) *MessageSenderHTTP {
	return &MessageSenderHTTP{config: config}
}

// FetchMPCVersion fetches the version of the attached MPC compute node
func (ms *MessageSenderHTTP) FetchMPCVersion() ([]byte, error) {
	urlPath := "/version"
	return ms.SendRequest("GET", urlPath, nil)
}

// FetchQueryResult fetches the result of the given query ID from the MPC node
func (ms *MessageSenderHTTP) FetchQueryResult(queryID string) ([]byte, error) {
	urlPath := fmt.Sprintf("/%v/query/%v/%v/result",
		viper.GetString("version"),
		"basicstats", // TODO: remove hardcoded value
		queryID)
	return ms.SendRequest("GET", urlPath, nil)
}

// FetchApprovalRequests fetches all pending approval requests
func (ms *MessageSenderHTTP) FetchApprovalRequests() ([]byte, error) {
	urlPath := fmt.Sprintf("/%v/approvals", viper.GetString("version"))
	return ms.SendRequest("GET", urlPath, nil)
}

// FetchApprovalRequests fetches all pending approval requests
func (ms *MessageSenderHTTP) FetchApprovalRequest(queryID string) ([]byte, error) {
	urlPath := fmt.Sprintf("/%v/approvals/%s", viper.GetString("version"), queryID)
	return ms.SendRequest("GET", urlPath, nil)
}

// SubmitApprovalResponse submits the response of an approval request
func (ms *MessageSenderHTTP) SubmitApprovalResponse(queryID string, approve bool) ([]byte, error) {
	urlPath := fmt.Sprintf("/%v/approvals/%v", viper.GetString("version"), queryID)
	body, err := json.Marshal(messages.JSONApprovalResponse{Approve: &approve})
	if err != nil {
		return []byte(""), err
	}
	return ms.SendRequest("POST", urlPath, &body)
}

// SendRequest sends a HTTP request to another MPC node
func (ms *MessageSenderHTTP) SendRequest(method string, urlPath string, reqBody *[]byte) ([]byte, error) {
	ms.Lock()
	if ms.client == nil {
		cli, err := createClient(ms.config, ms.mpcNodeConfig)
		if err == nil {
			ms.client = cli
		} else {
			log.Error().Msg("Failed to initialize HTTP client")
			ms.Unlock()
			return []byte(""), errors.New("Failed to intitialize HTTP client")
		}
	}
	ms.Unlock()

	receiver := ms.mpcNodeConfig

	connectionURL := fmt.Sprintf("https://%v:%d%v",
		receiver.Host,
		receiver.Port,
		urlPath)

	log.Trace().
		Str("receiver", receiver.Name).
		Str("method", method).
		Str("url", urlPath).
		Msgf("  [%s] <<< %s %s", receiver.Name, method, urlPath)

	var body *bytes.Reader
	if reqBody != nil {
		body = bytes.NewReader(*reqBody)
	}

	var resp *http.Response
	var err error

	switch method {
	case "POST":
		resp, err = ms.client.Post(
			connectionURL,
			"application/octet-stream",
			body)
	case "GET":
		resp, err = ms.client.Get(connectionURL)
	default:
		return []byte(""), fmt.Errorf("Unknown method %s", method)
	}
	if err != nil {
		log.Error().Err(err).
			Str("receiver", receiver.Name).
			Str("method", method).
			Str("url", urlPath).
			Msgf("  [%s] xxx %s %s", receiver.Name, method, urlPath)
		ms.Lock()
		ms.client = nil
		ms.Unlock()
		return []byte(""), err
	}

	defer resp.Body.Close()

	var event *zerolog.Event
	var httpError error
	switch code := resp.StatusCode; {
	case (code >= 400) && (code <= 599):
		event = log.Error()
		httpError = &HTTPError{StatusCode: resp.StatusCode}
	default:
		event = log.Trace()
	}
	event.
		Str("receiver", receiver.Name).
		Str("method", method).
		Str("url", urlPath).
		Int("status", resp.StatusCode).
		Msgf("  [%s] >>> %s %s %d", receiver.Name, "POST", urlPath, resp.StatusCode)

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msgf("Error reading body from http response")
		return []byte(""), err
	}
	log.Trace().Msgf("Response body: %v", string(respBody))

	return respBody, httpError
}

func createClient(config h.Config, otherNode h.Config) (*http.Client, error) {
	// Load certificates and keys
	cert, err := tls.LoadX509KeyPair(config.TLSCertFile, config.TLSKeyFile)
	if err != nil {
		log.Error().Err(err).Msg("Failed to load X509 Keypair")
		return nil, err
	}

	caCertPool := x509.NewCertPool()
	caCert, err := ioutil.ReadFile(otherNode.TLSCertFile)
	if err != nil {
		log.Error().Err(err).Msg("Failed to read TLS Certificate")
		return nil, err
	}
	if ok := caCertPool.AppendCertsFromPEM(caCert); !ok {
		log.Error().Str("otherNode", otherNode.Name).Msg("Failed to parse TLS Certificate")
		return nil, errors.New("Failed to parse TLS Certificate")
	}

	// Create TLS Config
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      caCertPool,
	}

	tr := &http.Transport{
		TLSClientConfig: tlsConfig,
		MaxIdleConns:    3,
		IdleConnTimeout: 30 * time.Second,
	}
	return &http.Client{Transport: tr}, nil
}

// NodeConfigurationUpdate implements the coordinator.NodeConfigurationUpdate method
func (ms *MessageSenderHTTP) NodeConfigurationUpdate(org, ip, port, cert, _ string) {
	if ms.config.Name != org {
		return
	}
	log.Debug().
		Str("org", org).
		Str("ip", ip).
		Msg("Received configuration update")
	err := os.MkdirAll(path.Join("tls", "ext"), 0744)
	if err != nil {
		log.Error().Err(err).Msg("Cannot create directory")
	}

	portInt, err := strconv.Atoi(port)
	if err != nil {
		log.Error().Err(err).Msg("Cannot convert port to integer")
	}

	if localIP, ok := extractLocalIP(cert); ok {
		log.Debug().
			Str("org", org).
			Str("local-ip", localIP).
			Str("public-ip", ip).
			Msg("Found a local network IP in the certificate. Will use local IP to fetch results from MPC node instead of the public IP.")
		ip = localIP
	}

	config := h.Config{
		Name:        org,
		Port:        portInt,
		Host:        ip,
		Role:        h.Node,
		TLSCertFile: path.Join("tls", "ext", org+".crt"),
		TLSKeyFile:  "",
	}

	ms.Lock()
	defer ms.Unlock()
	ms.mpcNodeConfig = config
	err = ioutil.WriteFile(config.TLSCertFile, []byte(cert), 0744)
	if err != nil {
		log.Error().Err(err).Msg("Failed to write certificate to file")
	}
	log.Info().
		Str("org", org).
		Str("ip", ip).
		Str("certSig", hex.EncodeToString(config.CertSignature())).
		Msg("Applying configuration update")
}

// extractLocalIP takes an x509 certificate and reads its IPAddresses member.
// If it contains multiple IP addresses, and one of them is a local network IP,
// it returns this IP address (as a string), and `true` as the second return value.
// If not, it returns an empty string and `false`.
func extractLocalIP(certStr string) (localIP string, ok bool) {
	var x509Cert *x509.Certificate

	certBytes := []byte(certStr)
	for {
		var decodedPart *pem.Block
		decodedPart, certBytes = pem.Decode(certBytes)
		if decodedPart == nil {
			return "", false
		}
		if decodedPart.Type == "CERTIFICATE" {
			var err error
			x509Cert, err = x509.ParseCertificate(decodedPart.Bytes)
			if err == nil {
				break
			}
		}
	}

	// We have a valid certificate!
	// See if any of the IPs in it are a private network IP
	// (see https://en.wikipedia.org/wiki/Private_network)
	for _, ip := range x509Cert.IPAddresses {
		ipv4 := ip.To4()
		if len(ipv4) == 4 && (ipv4[0] == 10 || (ipv4[0] == 172 && ipv4[1]&0xF0 == 0x10) || (ipv4[0] == 192 && ipv4[1] == 168)) {
			return ipv4.String(), true
		}
	}
	return "", false
}
