// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package transport

import (
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"ci.tno.nl/gitlab/ppa-project/pkg/http"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
)

// This test checks the sequence of inserting and extracting a local network IP
// from the TLS certificate posted to the blockchain.
func TestMain(m *testing.M) {
	zerolog.DurationFieldInteger = true
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, NoColor: true})

	code := m.Run()
	// Just to be sure
	os.Remove("tls.cert.file")
	os.Remove("tls.key.file")
	os.Exit(code)
}

func TestExtractLocalIP(t *testing.T) {
	testExtractLocalIPWithHosts(t, "canonical.host.name,222.22.2.22,192.168.12.13,12.13.14.15", true, "192.168.12.13")
	testExtractLocalIPWithHosts(t, "canonical.host.name,222.22.2.22,10.234.12.13,12.13.14.15", true, "10.234.12.13")
	testExtractLocalIPWithHosts(t, "canonical.host.name,222.22.2.22,172.19.12.13,12.13.14.15", true, "172.19.12.13")
	testExtractLocalIPWithHosts(t, "canonical.host.name,192.168.12.13", true, "192.168.12.13")
	testExtractLocalIPWithHosts(t, "192.168.12.13", true, "192.168.12.13")
	testExtractLocalIPWithHosts(t, "canonical.host.name,222.22.2.22,12.13.14.15", false, "")
	testExtractLocalIPWithHosts(t, "canonical.host.name,222.22.2.22,172.0.0.1", false, "")
	testExtractLocalIPWithHosts(t, "canonical.host.name", false, "")
}

func testExtractLocalIPWithHosts(t *testing.T, hosts string, shouldOk bool, shouldIP string) {
	viper.SetDefault("host", hosts)

	httpConfig := http.Config{
		Name:        "TestNode",
		Host:        strings.Split(viper.GetString("host"), ",")[0],
		BindHost:    "bind.host",
		Port:        1234,
		Role:        http.Self,
		TLSCertFile: "tls.cert.file",
		TLSKeyFile:  "tls.key.file",
	}

	// Daemon generates a new certificate as follows:
	http.SetupTLSCertificateWithAltNames(httpConfig, strings.Split(viper.GetString("host"), ",")[1:])

	// Now, this file is posted on the blockchain.
	blockchainCert, _ := ioutil.ReadFile("tls.cert.file")

	// Check if it worked!
	ip, ok := extractLocalIP(string(blockchainCert))
	assert.Equal(t, shouldOk, ok)
	assert.Equal(t, shouldIP, ip)

	os.Remove("tls.cert.file")
	os.Remove("tls.key.file")
}
