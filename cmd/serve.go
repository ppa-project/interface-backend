// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/signal"
	"path"
	"runtime/debug"
	"sort"
	"strings"
	"syscall"
	"time"

	"ci.tno.nl/gitlab/ppa-project/interface-backend/keystore"
	"ci.tno.nl/gitlab/ppa-project/interface-backend/logger"
	"ci.tno.nl/gitlab/ppa-project/interface-backend/rest"
	"ci.tno.nl/gitlab/ppa-project/interface-backend/service"
	"ci.tno.nl/gitlab/ppa-project/interface-backend/transport"
	"ci.tno.nl/gitlab/ppa-project/interface-backend/version"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator/etcd"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator/smartcontract"
	h "ci.tno.nl/gitlab/ppa-project/pkg/http"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "serve [nodename] [hostname]",
	Short: "Serve interface backend",
	Args:  cobra.RangeArgs(0, 2),
	Long: `Serve interface backend.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) >= 1 {
			viper.Set("nodename", args[0])
		}
		if len(args) >= 2 {
			viper.Set("host", args[0])
		}
		replaceEnv()
		printVersion()
		setDefaults()
		serve()
	},
}

func printVersion() {
	bi, ok := debug.ReadBuildInfo()
	if !ok {
		fmt.Printf("Failed to read build info")
	}

	if version.BuildDate != "" || version.GitCommit != "" || version.GitBranch != "" {
		fmt.Printf("Interface backend - Git: %s/%s - Build date: %s", version.GitBranch, version.GitCommit, version.BuildDate)
	} else {
		fmt.Printf("Interface backend - Dev")
	}

	if ok {
		if viper.GetBool("debug") {
			fmt.Println()
			for _, dep := range bi.Deps {
				if strings.HasPrefix(dep.Path, "ci.tno.nl") {
					fmt.Printf("%s version %s\n", dep.Path, dep.Version)
				}
			}
		} else {
			for _, dep := range bi.Deps {
				if dep.Path == "ci.tno.nl/gitlab/ppa-project/pkg/scbindings" {
					fmt.Printf(" - scbindings version %s\n", dep.Version)
				}
			}
		}
	}
}

func replaceEnv() {
	// Read variables from environment variables
	// BACKEND_TEST_VAR becomes test.var in Viper
	viper.SetEnvPrefix("backend")
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
	viper.AutomaticEnv()
}

func setDefaults() {
	// Set default values
	nodename := viper.GetString("nodename")
	viper.SetDefault("logfile", fmt.Sprintf("interface_backend-%s.log", nodename))
	viper.SetDefault("version", "v1") // TODO: Do we need this?

	viper.SetDefault("bindhost", "0.0.0.0")

	viper.SetDefault("tls.certfile", path.Join("tls", nodename+".crt"))
	viper.SetDefault("tls.keyfile", path.Join("tls", nodename+".key"))

	// TODO: move this to the coordinator?
	// ethereum.privatekey
	viper.SetDefault("ethereum.privatekey", "")
	viper.SetDefault("ethereum.keyfile", "ethereumkey")
	viper.SetDefault("ethereum.url", "ws://0.0.0.0:0")
	viper.SetDefault("ethereum.smartcontractaddress", "")
	// ethereum.smartcontract.address

	viper.SetDefault("quorum", true)

	// TODO: move this to the coordinator?
	viper.SetDefault("etcd.host", "127.0.0.1")
	viper.SetDefault("etcd.port", 2379)
	viper.SetDefault("etcd.cacert", "")
	viper.SetDefault("etcd.clientcert", "")
	viper.SetDefault("etcd.clientkey", "")

	// Workaround to support unmarshaling of environment variables
	keys := viper.AllKeys()
	sort.Strings(keys)
	for _, key := range keys {
		val := viper.Get(key)
		viper.Set(key, val)
		if viper.GetBool("debug") {
			fmt.Printf("Using variable %v = %v\n", key, val)
		}
	}
}

var verbose bool

func init() {
	startCmd.PersistentFlags().Int("port", 3000, "Port number")
	viper.BindPFlag("port", startCmd.PersistentFlags().Lookup("port"))

	startCmd.PersistentFlags().String("coordinator", "smartcontract", "Coordinator")
	viper.BindPFlag("coordinator", startCmd.PersistentFlags().Lookup("coordinator"))

	rootCmd.AddCommand(startCmd)
}

func serve() {
	logger.InitLog()
	thisNodeName := viper.GetString("nodename")

	httpConfig := GetOwnHTTPConfig()
	h.SetupTLSCertificate(httpConfig)

	ms := transport.NewMessageSenderHTTP(httpConfig)

	// Coordinator
	var coor coordinator.Coordinator
	viper.SetDefault("coordinator", "smartcontract")
	coord := viper.GetString("coordinator")

	switch coord {
	case "smartcontract":
		// Set up the smart contract binding
		scConfig := GetSCConfig(thisNodeName)
		coor = smartcontract.NewCoordinator(scConfig)
	case "etcd":
		etcdConfig := GetEtcdConfig(thisNodeName)
		coor = etcd.NewCoordinator(etcdConfig, false)
	default:
		log.Fatal().Msgf("Unknown coordinator: %s", coord)
	}

	coor.RegisterNodeConfigListener(ms.NodeConfigurationUpdate)

	err := coor.ConnectAndListen() // Non-blocking call
	if err != nil {
		log.Fatal().Err(err).Str("coordinator", coord).Msg("Failed to run coordinator")
	} else {
		go UpdateGUIConfig(coor, httpConfig)
	}

	service := service.NewService(coor, ms)

	rc := rest.NewRESTController(&service)
	coor.RegisterAuditLogEntryListener(rc.NewAuditLogEntry)
	srv := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", viper.GetString("bindhost"), httpConfig.Port),
		Handler: rc.GetRouter(),
	}

	go func() {
		log.Info().Msgf("Starting HTTPS server on port %d", httpConfig.Port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatal().Err(err).Msg("Failed to run HTTP Server")
		}
	}()

	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)    // SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, syscall.SIGTERM) // SIGTERM signal sent from Docker/Kubernetes

	// Block until we receive our signal.
	<-c
	log.Info().Msg("Received interrupt, shutting down...")

	// Gracefully shutdown HTTPServer
	closeChan := make(chan struct{})
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	go func() {
		if err := srv.Shutdown(ctx); err != nil {
			// Error from closing listeners, or context timeout:
			log.Warn().Err(err).Msg("Failed to graceful shutdown HTTP Server")
		}
		close(closeChan)
	}()

	// Blocks until the HTTP server is shutdown.
	<-closeChan
}

// GetOwnHTTPConfig returns a HTTPConfig based on the configuration in viper
func GetOwnHTTPConfig() h.Config {
	return h.Config{
		Name:        viper.GetString("nodename"),
		Host:        viper.GetString("host"),
		Port:        viper.GetInt("port"),
		Role:        h.Self,
		TLSCertFile: viper.GetString("tls.certfile"),
		TLSKeyFile:  viper.GetString("tls.keyfile"),
	}
}

func GetSCConfig(thisNodeName string) coordinator.Config {
	httpConfig := GetOwnHTTPConfig()
	ourIP := httpConfig.Host
	ourCert := httpConfig.TLSCertFile

	var scConfig coordinator.Config
	scConfig.Organization = thisNodeName
	scConfig.Ip = ourIP

	certfile, err := ioutil.ReadFile(ourCert)
	if err != nil {
		log.Fatal().Err(err).Str("node", thisNodeName).Msg("Failed to read TLS Certificate")
	}
	scConfig.Cert = string(certfile)

	scConfig.EthereumPrivateKey = keystore.GetEthereumPrivateKey()
	scConfig.EthereumURL = viper.GetString("ethereum.url")
	scConfig.EthereumContractAddress = viper.GetString("ethereum.smartcontractaddress")

	return scConfig
}

func GetEtcdConfig(thisNodeName string) etcd.Config {
	viper.SetDefault("etcd.host", "localhost")
	viper.SetDefault("etcd.port", "2379")

	return etcd.Config{
		Organization:   thisNodeName,
		EtcdHost:       viper.GetString("etcd.host"),
		EtcdPort:       viper.GetString("etcd.port"),
		EtcdCACert:     viper.GetString("etcd.cacert"),
		EtcdClientCert: viper.GetString("etcd.clientcert"),
		EtcdClientKey:  viper.GetString("etcd.clientkey"),
	}
}

func UpdateGUIConfig(coor coordinator.Coordinator, config h.Config) {
	caCert, _ := ioutil.ReadFile(config.TLSCertFile)
	if err := coor.UpdateGuiConfig(config.Host, string(caCert)); err != nil {
		log.Error().Err(err).Msg("Set failed")
	}
}
