// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"ci.tno.nl/gitlab/ppa-project/interface-backend/transport"
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"ci.tno.nl/gitlab/ppa-project/pkg/types"
	"github.com/ethereum/go-ethereum/common"
)

const (
	// PageLength controls how many audit logs are sent per page
	// TODO: unreasonably high, it was 100. The front-end only requests the first page.
	// This can be reset to 100 once the front-end requests all pages as needed.
	PageLength = 100000
)

type Service struct {
	coor coordinator.Coordinator
	ms   transport.MessageSender
}

func NewService(coor coordinator.Coordinator, ms transport.MessageSender) Service {
	return Service{coor: coor, ms: ms}
}

func (s *Service) FetchMPCVersion() ([]byte, error) {
	result, err := s.ms.FetchMPCVersion()
	if err != nil {
		return []byte(""), err
	}
	return result, nil
}

func (s *Service) ProcessNewQuery(newQuery types.Query, newMetadata coordinator.NewQueryMetadata) (string, error) {
	queryID, err := s.coor.SubmitNewQuery(newQuery, newMetadata)
	if err != nil {
		return "", err
	}
	return queryID, nil
}

func (s *Service) GetQueries() []string {
	result := make([]string, 0)
	kind := coordinator.QueryApproved
	len := s.coor.AuditLogLen(kind)
	logEntries := s.coor.AuditLog(kind, 0, len)
	for _, entry := range logEntries {
		queryID := entry.Contents.(coordinator.LogEntryQueryApproved).Query.ID
		result = append(result, queryID)
	}
	return result
}

func (s *Service) GetQueryDetails(queryID string) (*coordinator.QueryDetails, error) {
	queryDetails, err := s.coor.GetQueryDetails(queryID)
	if err != nil {
		return nil, err
	}
	return &queryDetails, nil
}

func (s *Service) FetchQueryResult(queryID string) ([]byte, error) {
	result, err := s.ms.FetchQueryResult(queryID)
	if err != nil {
		return []byte(""), err
	}
	return result, nil
}

func (s *Service) GetAuditLogs() interface{} {
	return s.GetAuditLogsWithPage(0)
}

func (s *Service) GetAuditLogsWithPage(page int) interface{} {
	logEntries := s.coor.AuditLog(coordinator.Any, page*PageLength, PageLength)
	return logEntries
}

func (s *Service) GetTrailingAuditLogs(entries int, kind coordinator.LogKind) []coordinator.LogEntry {
	len := s.coor.AuditLogLen(kind)
	logEntries := s.coor.AuditLog(kind, len-entries, entries)
	return logEntries
}

func (s *Service) GetAuditLogsWithQueryID(queryID string) interface{} {
	return s.GetAuditLogsPageWithQueryID(queryID, 0)
}

func (s *Service) GetAuditLogsPageWithQueryID(queryID string, page int) interface{} {
	logEntries := s.coor.AuditLog(coordinator.QueryApproved|coordinator.QueryComplete|coordinator.QueryRejected|coordinator.QueryUnaffordable, page*PageLength, PageLength)
	entries := make([]coordinator.LogEntry, 0)
	for _, entry := range logEntries {
		if approved, ok := entry.Contents.(coordinator.LogEntryQueryApproved); ok {
			if approved.Query.ID == queryID {
				entries = append(entries, entry)
			}
		} else if completed, ok := entry.Contents.(coordinator.LogEntryQueryComplete); ok {
			if common.Hash(completed.QueryID).Hex()[2:] == queryID {
				entries = append(entries, entry)
			}
		}
	}
	return entries
}

func (s *Service) GetBusinessRules() []string {
	return s.coor.GetBusinessRules()
}

func (s *Service) GetOrganizations() ([]string, error) {
	return s.coor.GetOrganizations()
}

func (s *Service) GetAttributes(organization string) (types.DatabaseAttributes, error) {
	presence, err := s.coor.GetNodeConfig(organization)
	if err != nil {
		return nil, err
	}
	return presence.Attributes, nil
}
