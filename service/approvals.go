// Copyright 2021 TNO
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"ci.tno.nl/gitlab/ppa-project/pkg/coordinator"
	"github.com/ethereum/go-ethereum/common"
)

func (s *Service) FetchApprovalRequests() ([]byte, error) {
	result, err := s.ms.FetchApprovalRequests()
	if err != nil {
		return []byte(""), err
	}
	return result, nil
}

func (s *Service) FetchApprovalRequest(queryID string) ([]byte, error) {
	result, err := s.ms.FetchApprovalRequest(queryID)
	if err != nil {
		return []byte(""), err
	}
	return result, nil
}

func (s *Service) SubmitApprovalResponse(queryID string, approve bool) ([]byte, error) {
	result, err := s.ms.SubmitApprovalResponse(queryID, approve)
	if err != nil {
		return []byte(""), err
	}
	return result, nil
}

func (s *Service) GetManualReviewLogEntries() map[string]coordinator.LogEntry {
	result := make(map[string]coordinator.LogEntry)
	kind := coordinator.QueryManualReview
	len := s.coor.AuditLogLen(kind)
	logEntries := s.coor.AuditLog(kind, 0, len)
	for _, entry := range logEntries {
		queryID := entry.Contents.(coordinator.LogEntryQueryManualReview).QueryID
		queryIDHex := common.Hash(queryID).Hex()[2:]
		result[queryIDHex] = entry
	}
	return result
}

func (s *Service) GetQueryCompleteLogEntries() map[string]coordinator.LogEntry {
	result := make(map[string]coordinator.LogEntry)
	kind := coordinator.QueryComplete
	len := s.coor.AuditLogLen(kind)
	logEntries := s.coor.AuditLog(kind, 0, len)
	for _, entry := range logEntries {
		queryID := entry.Contents.(coordinator.LogEntryQueryComplete).QueryID
		queryIDHex := common.Hash(queryID).Hex()[2:]
		result[queryIDHex] = entry
	}
	return result
}
