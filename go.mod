module ci.tno.nl/gitlab/ppa-project/interface-backend

go 1.16

require (
	ci.tno.nl/gitlab/ppa-project/pkg/coordinator v1.0.16
	ci.tno.nl/gitlab/ppa-project/pkg/http v1.0.2
	ci.tno.nl/gitlab/ppa-project/pkg/types v1.0.4
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/ethereum/go-ethereum v1.9.25
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/websocket v1.4.2
	github.com/ianlancetaylor/cgosymbolizer v0.0.0-20210303021718-7cb7081f6b86
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.6.1
	github.com/swaggo/files v0.0.0-20190704085106-630677cd5c14
	github.com/swaggo/gin-swagger v1.2.0
	github.com/swaggo/swag v1.7.0
)

// https://stackoverflow.com/questions/61093734/go-get-is-pulling-the-wrong-repository
replace (
	ci.tno.nl/gitlab/ppa-project/pkg/coordinator => ci.tno.nl/gitlab/ppa-project/pkg/coordinator.git v1.0.16
	ci.tno.nl/gitlab/ppa-project/pkg/http => ci.tno.nl/gitlab/ppa-project/pkg/http.git v1.0.2
	ci.tno.nl/gitlab/ppa-project/pkg/scbindings => ci.tno.nl/gitlab/ppa-project/pkg/scbindings.git v1.0.14
	ci.tno.nl/gitlab/ppa-project/pkg/types => ci.tno.nl/gitlab/ppa-project/pkg/types.git v1.0.4
	github.com/ethereum/go-ethereum => ci.tno.nl/gitlab/ppa-project/pkg/go-ethereum.git v1.9.25-ppa-1
)
